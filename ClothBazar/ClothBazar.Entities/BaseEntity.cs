﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothBazar.Entities
{
   public  class BaseEntity
    {
        public int ID { get; set; }
        public int Name { get; set; }
        public int Description { get; set; }
    }
}
