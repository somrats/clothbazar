﻿using ClothBazar.Database;
using ClothBazar.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothBazar.Services
{
   public class CategoriesServices
    {
        public List<Category> GetCategories()
        {
            using (var context = new CBContext())
            {
                return context.Catagories.ToList();
            }
        }

        public void SaveCategory(Category category)
            {
                using (var context = new CBContext())
                {
                    context.Catagories.Add(category);
                    context.SaveChanges();

                }
            }
    }
}
