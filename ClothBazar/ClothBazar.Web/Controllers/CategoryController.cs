﻿using ClothBazar.Entities;
using ClothBazar.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
namespace ClothBazar.Web.Controllers
{
    public class CategoryController : Controller
    {
        CategoriesServices categoryService = new CategoriesServices();
       

        [HttpGet]
        public ActionResult Index()
        {
            var categories = categoryService.GetCategories();
            List<Category> cats = categoryService.GetCategories();

            return View(categories);
        }

        [HttpGet]
        public ActionResult Create()
        {
          
            return View();
        }


        [HttpPost]
        public ActionResult Create(Category category)
        {
            categoryService.SaveCategory(category);
            return View();
        }
    }
}